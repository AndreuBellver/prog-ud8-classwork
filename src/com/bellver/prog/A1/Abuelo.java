package com.bellver.prog.A1;

public class Abuelo {

    private String name;

    public Abuelo(){

        this.name="Sin Nombre";

    }

    public Abuelo(String name){

        this.name = name;

    }

    public String toString() {
        return "name: " + name;
    }
}
