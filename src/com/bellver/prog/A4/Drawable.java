package com.bellver.prog.A4;

public interface Drawable {

    public String getDrawing();

    public String getDrawing(Color color);

    public void setColor(Color color);

}
