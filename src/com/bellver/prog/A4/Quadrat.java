package com.bellver.prog.A4;

public class Quadrat extends Figura{

    private double costat;

    public Quadrat(String name, double area, double perimetro, double costat) {

        super(name, area, perimetro);
        this.costat = costat;

    }

    @Override
    public double getArea() {

        return costat * costat;
    }

    @Override
    public double getPerimetro() {

        return costat*4;
    }

    @Override
    public void setColor(Color color) {

        super.setColor(color);

    }

    @Override
    public String toString() {

        return super.toString() + "\n" + costat;

    }
}
