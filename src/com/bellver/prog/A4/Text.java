package com.bellver.prog.A4;

public class Text implements Drawable {

    private String name;
    private String argument;
    private Color color;

    public Text(String name, String argument) {

        this.argument = argument;
        this.name = name;
    }

    public int getLenght() {

        return argument.length();

    }

    @Override
    public String toString() {

        return "Text{" +
                "name='" + name + '\'' +
                ", argument='" + argument + '\'' +
                '}';

    }

    @Override
    public String getDrawing() {

        return toString();
    }

    @Override
    public String getDrawing(Color color) {
        return color + toString();
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}
