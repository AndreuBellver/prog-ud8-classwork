package com.bellver.prog.A4;

public class Rectangle extends Figura{

    private double base;
    private double altura;

    public Rectangle(String name, double area, double perimetro, double base, double altura) {

        super(name, area, perimetro);
        this.base = base;
        this.altura = altura;

    }

    @Override
    public double getArea() {

        return base * altura;
    }

    @Override
    public double getPerimetro() {

        return base*2 + altura*2;
    }

    @Override
    public void setColor(Color color) {

        super.setColor(color);

    }

    @Override
    public String toString() {

        return super.toString() + "\nBase: " + base +
                                "\nAltura: " + altura;

    }
}
