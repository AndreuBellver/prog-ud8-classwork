package com.bellver.prog.A4;

abstract class Figura implements Drawable{

    private String name;
    private double area;
    private double perimetro;
    protected Color color;

    public Figura(String name, double area, double perimetro) {

        this.name = name;
        this.area = area;
        this.perimetro = perimetro;
        this.color = Color.BLACK;

    }

    @Override
    public String getDrawing() {

      return toString();

    }

    @Override
    public String getDrawing(Color color) {

        return color + toString();

    }

    @Override
    public void setColor(Color color) {

    }

    public double getArea() {
        return area;
    }

    public double getPerimetro() {

        return perimetro;

    }


    @Override
    public String toString() {
        return "Figura{" +
                "name='" + name + '\'' +
                ", area=" + area +
                ", perimetro=" + perimetro +
                '}';

    }
}
