package com.bellver.prog.A4;

import java.util.ArrayList;

public class Pizarra {

    private ArrayList<Drawable> figures;

    public Pizarra(){

        this.figures = new ArrayList<>();

    }

    public void Draw(){

        for (Drawable Figura: figures) {

            System.out.println(Figura);

        }

    }

    public void Clear(){

        figures.removeAll(figures);

    }
}
