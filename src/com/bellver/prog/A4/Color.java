package com.bellver.prog.A4;

public enum Color {

    BLACK {
        @Override
        public String toString() {
            return "\u001B[30m";
        }

    }, RED {
        @Override
        public String toString() {
            return "\u001B[31m";

        }
    }, GREEN {
        @Override
        public String toString() {
            return "\u001B[32m";
        }

    }, YELLOW {
        @Override
        public String toString() {
            return "\u001B[30m";
        }
    }
}

