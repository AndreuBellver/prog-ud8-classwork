package com.bellver.prog.A4;

public class Circulo extends Figura{

    private double radi;
    private double altura;

    public Circulo(String name, double area, double perimetro, double radi) {

        super(name, area, perimetro);
        this.radi = radi;

    }

    @Override
    public double getArea() {

        return  Math.PI*(radi * radi);
    }

    @Override
    public double getPerimetro() {

        return 2 * Math.PI * radi;
    }

    @Override
    public void setColor(Color color) {

        super.setColor(color);

    }

    @Override
    public String toString() {

        return super.toString() + "\nRadi: " + radi;
    }
}
