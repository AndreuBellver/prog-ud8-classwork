package com.bellver.prog.A3;

abstract class Vehicle {

    protected String matricula;
    protected String color;
    protected String model;
    protected String marca;

    public Vehicle(String matricula, String color, String model, String marca){

            this.marca = marca;
            this.color = color;
            this.matricula = matricula;
            this.model = model;

    }

    public String getMatricula() {

        return matricula;
    }
}
