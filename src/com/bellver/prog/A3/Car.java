package com.bellver.prog.A3;

public class Car extends Vehicle{

    private int cilindrada;

    public Car(int cilindrada, String matricula, String color, String model, String marca) {

        super(matricula,color,model,marca);

        this.cilindrada = cilindrada;

    }

    @Override
    public String toString() {
        return "\nMatricula: " + matricula + "\n" +
                "Marca: " + marca + "\n" +
                "Model: " + model + "\n" +
                "Cilindrada: " + cilindrada + "\n";
    }
}
