package com.bellver.prog.A3;

import java.util.ArrayList;

public class Garage {

    private ArrayList<Vehicle> repaired;
    private ArrayList<Vehicle> pending;


    public Garage() {

        this.repaired = new ArrayList<>();
        this.pending = new ArrayList<>();

    }

    public void registerRepair(String matricula){

        Vehicle repariredVehicle = this.getPendingVehicle(matricula);

        if (repariredVehicle != null) {

            this.pending.remove(repariredVehicle);
            this.repaired.add(repariredVehicle);

        }
    }

    public void registerNewVehicle(Vehicle vehicle){

        this.pending.add(vehicle);

    }

    private Vehicle getRepairedVehicle(String licensePlate ){

        for (int i = 0; i< repaired.size(); i++){

            Vehicle coche = repaired.get(i);

            if (coche.getMatricula().equalsIgnoreCase(licensePlate)){

                return coche;
            }
        }

        return null;
    }

    public Vehicle takeOffVehicle(String matricula){

        Vehicle out = this.getRepairedVehicle(matricula);

        if (out != null) {

            this.repaired.remove(out);
        }

        return out;
    }

    private Vehicle getPendingVehicle(String matricula ){

        for (int i = 0; i< pending.size(); i++){

            Vehicle pendingVehicle = pending.get(i);

            if (pendingVehicle.getMatricula().equalsIgnoreCase(matricula)){

                return pendingVehicle;

            }
        }

        return null;
    }

    public void showPendingVehicle(){


        for (Vehicle Pending: pending) {

            System.out.println(Pending);

        }

    }

    public void showRepairedCars(){

        for (Vehicle Repaired: repaired) {

            System.out.println(Repaired);

        }

    }

}
