package com.bellver.prog.A3;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;

    private Garage garage;

    private final int OP_REGISTER = 1;
    private final int OP_PENDING = 2;
    private final int OP_REPAIRED = 3;
    private final int OP_SETASAREPAIRED = 4;
    private final int OP_SETEXITCAR = 5;
    private final int OP_EXIT = 6;

    private final int OP_CAR = 1;
    private final int OP_CAMIO = 2;
    private final int OP_MOTO = 3;
    private final int OP_BICICLETA = 4;

    public Menu(){
        scanner = new Scanner(System.in);
        garage = new Garage();
    }

    public void mainMenu() {

        int respuesta;

        do{

        System.out.print("\n\nMenu:" +
                "\n======" +
                "\n1.- Register New Vehicle" +
                "\n2.- Pending to repair" +
                "\n3.- Repared Vehicles" +
                "\n4.- Set vegicle as a Repair" +
                "\n5.- Set vechicle's exit" +
                "\n6.- Close Program\n\n");

        respuesta = scanner.nextInt();

        switch (respuesta){
            case OP_REGISTER:
                registerNewVehicle();
                break;

            case OP_PENDING:
                garage.showPendingVehicle();
                break;

            case OP_REPAIRED:
                garage.showRepairedCars();
                break;

            case OP_SETASAREPAIRED:
                garage.showPendingVehicle();
                garage.registerRepair(getMatricula());
                break;

            case OP_SETEXITCAR:
                garage.showRepairedCars();
                garage.takeOffVehicle(getMatricula());
                break;
        }

      } while (respuesta!=OP_EXIT);

    }

    private void registerNewVehicle(){

        int respuesta;

        do{

            System.out.print("\n\nMenu:" +
                    "\n======" +
                    "\n" + OP_CAR + ".- Coche." +
                    "\n" + OP_CAMIO + ".- Camio" +
                    "\n" + OP_MOTO + ".- Motocicleta" +
                    "\n" + OP_BICICLETA + ".- Bicicleta");


            respuesta = scanner.nextInt();

            switch (respuesta){

                case OP_CAR:

                    Vehicle newCar = new Car(getCilindrada(),getMatricula(),getColor(),getModel(),getMarca());
                    garage.registerNewVehicle(newCar);
                    break;

                case OP_CAMIO:

                    Vehicle newCamio = new Camion(getCilindrada(),getMatricula(),getColor(),getModel(),getMarca());
                    garage.registerNewVehicle(newCamio);
                    break;

                case OP_MOTO:

                    Vehicle newMoto = new Motocicleta(getCilindrada(),getMatricula(),getColor(),getModel(),getMarca());
                    garage.registerNewVehicle(newMoto);
                    break;

                case OP_BICICLETA:

                    Vehicle newBici = new Bicicleta(getMatricula(),getColor(),getModel(),getMarca());
                    garage.registerNewVehicle(newBici);
                    break;

            }

        } while (respuesta!=OP_EXIT);

    }

    private String getMatricula() {

        System.out.print("\nMatricula:");
        return scanner.next();

    }

    private String getColor() {

        System.out.print("Color:");
        return scanner.next();

    }

    private int getCilindrada() {

        System.out.print("Cilindrada:");
        return scanner.nextInt();

    }


    public String getMarca() {

        System.out.print("Marca:");
        return scanner.next();

    }

    private String getModel() {

        System.out.print("Modelo:");
        return scanner.next();

    }

}
