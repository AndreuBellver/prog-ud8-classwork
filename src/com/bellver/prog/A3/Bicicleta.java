package com.bellver.prog.A3;

public class Bicicleta extends Vehicle{

    public Bicicleta(String matricula, String color, String model, String marca) {

        super(matricula,color,model,marca);

    }

    @Override
    public String toString() {
        return "\nMatricula: " + matricula + "\n" +
                "Marca: " + marca + "\n" +
                "Model: " + model + "\n";

    }
}
