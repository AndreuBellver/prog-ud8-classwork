package com.bellver.prog.A3;

public class Motocicleta extends Vehicle{

    private int cilindrada;

    public Motocicleta(int cilindrada,
                       String matricula, String color, String model, String marca) {

        super(matricula,color,model,marca);

        this.cilindrada = cilindrada;

    }

    @Override
    public String toString() {
        return "\nMatricula: " + matricula + "\n" +
                "Marca: " + marca + "\n" +
                "Model: " + model + "\n" +
                "Cilindrada: " + cilindrada + "\n";}
}
