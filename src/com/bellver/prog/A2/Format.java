package com.bellver.prog.A2;


public enum Format {

    WAV {
        @Override
        public String toString() {

            return "WAV";

        }

    }, MP3 {
        @Override
        public String toString() {

            return "MP3";

        }

    }, MIDI {
        @Override
        public String toString() {

            return "MIDI";

        }

    }, AVI {
        @Override
        public String toString() {

            return "AVI";

        }

    }, MOV {
        @Override
        public String toString() {

            return "MOV";

        }

    }, MPG {
        @Override
        public String toString() {

            return "MPG";

        }

    }, CDAUDIO {
        @Override
        public String toString() {

            return "CDAUDIO";

        }

    }, DVD {
        @Override
        public String toString() {

            return "DVD";

        }

    };
}
