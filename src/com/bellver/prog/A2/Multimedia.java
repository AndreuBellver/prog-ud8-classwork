package com.bellver.prog.A2;

abstract class Multimedia {

    protected String titol;
    protected String autor;
    protected Format format;
    protected String durada;

    public Multimedia(String titol, String autor, Format format, String durada) {

        this.titol = titol;
        this.autor = autor;
        this.format = format;
        this.durada = durada;

    }
}
