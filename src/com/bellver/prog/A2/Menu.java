package com.bellver.prog.A2;

import java.util.Scanner;

public class Menu {

    private ListPelicules catalogo;
    private Scanner scanner;

    private final int OP_INSERT = 1;
    private final int OP_SHOW = 2;
    private final int OP_EXIT = 3;

    public Menu() {

        this.catalogo = new ListPelicules();
        this.scanner = new Scanner(System.in);

    }

    public void startMenu() {

        System.out.println("\n\n\nVideo Club Habanna:");
        System.out.println("====================");

        System.out.print("\n" +
                OP_INSERT + ".- Insertar Pelicula\n" +
                OP_SHOW + ".- Mostrar Pelicules\n" +
                OP_EXIT + ".- Exit\n");

        int eleccio;

        do {

            eleccio = scanner.nextInt();

            if (eleccio == OP_INSERT) {

                insertPelicula();
                startMenu();

            } else if (eleccio == OP_SHOW) {

                System.out.println("\n\nCatalogo:");
                catalogo.showCatalogo();
                startMenu();

            } else {

                System.out.print("\nRespuesta incorrecta, vuelva a intentarlo.");

            }

        } while (eleccio != OP_EXIT);

    }

    private void insertPelicula() {

        System.out.println("Titol");
        String titol = scanner.next();

        System.out.println("Autor");
        String autor = scanner.next();

        System.out.println("Durada");
        String durada = scanner.next();

        System.out.println("Format");

        System.out.print("( " +   showFormatValues() + ").");

        String format = scanner.next();

        Format format1 = Format.valueOf(format.toUpperCase());

        do {

            System.out.println("Actriu Principal(N to null):");
            String actorPrincipal = scanner.next();

            System.out.println("Actriu Principal(N to null):");
            String actriuPrincipal = scanner.next();

            if (!actorPrincipal.equalsIgnoreCase("N") || !actriuPrincipal.equalsIgnoreCase("N")) {

                actorPrincipal = setNullIfN(actorPrincipal);
                actriuPrincipal = setNullIfN(actriuPrincipal);

                Pelicula newPelicula = new Pelicula(titol, autor, format1, durada, actorPrincipal, actriuPrincipal);

                catalogo.add(newPelicula);

                return;

            } else {

                System.out.println("Los parametros de \"Actriz Principal\" y \"Actor Principal\" no pueden ser nulos ambos. ");

            }

        } while (true);

    }

    private String setNullIfN(String args){

        if (args.equalsIgnoreCase("N")){

            args = " ";

            return args;

        } else {

            return args;
        }

    }

    private String showFormatValues (){

        Format[] values = Format.values();
        String args = "";

        for (int x = 0; x < values.length; x++){

            args = args + values[x] + " ";

        }

        return args;
    }

}
