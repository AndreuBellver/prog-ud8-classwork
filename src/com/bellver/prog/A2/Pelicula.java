package com.bellver.prog.A2;



public class Pelicula extends Multimedia{

    private String actorPrincipal;
    private String actriuPrincipal;

    public Pelicula(String titol, String autor, Format format, String durada,
                    String actorPrincipal, String actriuPrincipal) {

        super(titol, autor, format, durada);

        this.actorPrincipal = actorPrincipal;
        this.actriuPrincipal = actriuPrincipal;

    }

    @Override
    public boolean equals(Object obj) {

        Pelicula otherPelicula = (Pelicula) obj;

        return this.titol.equalsIgnoreCase(otherPelicula.titol)
                && this.autor.equalsIgnoreCase(otherPelicula.autor);

    }

    @Override
    public String toString() {

        return "\nTitol: " + super.titol +
                "\nAutor: " + super.autor +
                "\nDurada: " + super.durada +
                "\nFormat: " + super.format +
                "\nActor Principal: " + actorPrincipal +
                "\nActriu Principal: " + actriuPrincipal + "\n\n";
    }

}
