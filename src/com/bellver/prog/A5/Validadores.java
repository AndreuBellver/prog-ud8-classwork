package com.bellver.prog.A5;

public class Validadores {

    public boolean isValidProductCodeSingle(String code){

        if (code.matches("[0-999]")){
            return true;
        }
        return false;
    }
    public boolean isValidProductCodeMultiple(String code){

        if (code.matches("[0-999][ ][0-999]")){
            return true;
        }
        return false;
    }

}
